import pandas as pd
import os
import glob

os.makedirs('data/result', exist_ok=True)
path = os.path.join("data", "/*.csv")

all_files = glob.glob(os.path.join("data" , "*.csv"))

li = []

for filename in all_files:
    df = pd.read_csv(filename, index_col=None, header=0)
    li.append(df)

frame = pd.concat(li, axis=0, ignore_index=True)
frame.to_csv('data/result/out.csv', index=False,)