create virtual env

    python3 -m venv env

activate env
    
    source env/bin/activate

install all requirements
    
    pip install requirements.txt

create folder data and put to it all csv files

run code with command

    python merge.py

result will be add into data/result/out.csv file